﻿
$(function () {
    $("#header").load("navigationPanel.html");
    $('#textPara').load("../extras/content.txt");
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    btn.onclick = function() {
	modal.style.display = "block";
    }
    span.onclick = function() {
	modal.style.display = "none";
    }
    window.onclick = function(event) {
	if (event.target == modal) {
            modal.style.display = "none";
	}
    }   
});
function register() {
    $(".register").css("visibility", "visible");
    $(".logIn").css("visibility", "hidden");
}
function login() {
    $(".register").css("visibility", "hidden");
    $(".signIn").css("visibility", "visible");
    $(".selectUsername").css("visibility", "hidden");
}
function selectUsrName() {
    $(".selectUsername").css("visibility", "visible");
    $(".register").css("visibility", "hidden");

}
function hideNav() {
    $(".Header").css("visibility","hidden");
}

function showNav() {
    $(".Header").css("visibility","visible");
}
function newDoc() {
    window.location.assign("http://www.w3schools.com")
}
function newDoc1() {
    window.location.assign("logIn.html")
}
